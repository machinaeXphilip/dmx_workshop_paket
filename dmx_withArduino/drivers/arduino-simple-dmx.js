"use strict"

var SerialPort = require("serialport")

function ArduinoSimpleDmx(device_id, options) {
	var self = this
	options = options || {}

	self.maxChannel = 64

	this.universe = new Buffer(self.maxChannel+1)
	this.universe.fill(0)

	self.interval = 46

	this.dev = new SerialPort(device_id, {
		'baudRate': 115200
	}, function(err) {
		if(err) {
			console.log(err)
			return
		}
		self.start()
	})
}

ArduinoSimpleDmx.prototype.send_universe = function() {
	var self = this
	if(!this.dev.writable) {
		return
	}

	// toggle break
	self.dev.set({brk: true, rts: true}, function(err, r) {
		setTimeout(function() {
			self.dev.set({brk: false, rts: true}, function(err, r) {
				setTimeout(function() {
					//self.dev.write(Buffer.concat([Buffer([0]), self.universe.slice(1)]))
					//console.log(self.universe.slice(1)[0].toString());
					let uniNow = self.universe.slice(1);
					let uniString = [];
					let cmd = "";
					for(var i=0; i < self.maxChannel; i++)
					{
						let channel = uniNow[i];
						uniString[i] = channel.toString();
						cmd = cmd + (i+1) + "c" + channel.toString() + "w";
					}
					//console.log(cmd);

					//self.dev.write('1c255w2c0w');
					self.dev.write(cmd);
				}, 1)
			})
		}, 1)
	})
}

ArduinoSimpleDmx.prototype.start = function() {
	this.intervalhandle = setInterval(this.send_universe.bind(this), this.interval)
}

ArduinoSimpleDmx.prototype.stop = function() {
	clearInterval(this.intervalhandle)
}

ArduinoSimpleDmx.prototype.close = function(cb) {
	this.stop()
	this.dev.close(cb)
}

ArduinoSimpleDmx.prototype.update = function(u) {
	for(var c in u) {
		this.universe[c] = u[c]
	}
}

ArduinoSimpleDmx.prototype.updateAll = function(v) {
	for(var i = 1; i <= self.maxChannel; i++) {
		this.universe[i] = v
	}
}

ArduinoSimpleDmx.prototype.get = function(c) {
	return this.universe[c]
}

module.exports = ArduinoSimpleDmx
